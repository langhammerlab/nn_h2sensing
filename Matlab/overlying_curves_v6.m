function [] = overlying_curves_v6(slices,RelHumidities,sbp,norm,avg_factor,Quantity)
%% overlying curves
% figure;

subplot(sbp(1),sbp(2),sbp(3));
colors = lines(max(size(slices)));

for i = 1:max(size(slices))
    %% find the deoffsetting point
    D  = diff(slices{i}(:,4));
    CV = abs([D ; 0]) >= 0.01; % change vector, marks the position where the changes in hydrogen concentration occur (marks the last point before the change)    
    
    j = 1;
    % find the first H2 step
    while CV(j) ~= 1
        j = j + 1;
    end
    
    %% Visualise
    yyaxis left
    plot((slices{i}(:,1)-slices{i}(1,1))/60,movmean(slices{i}(:,2)-norm*mean(slices{i}(j-avg_factor+1:j,2)),avg_factor),'-','color',colors(i,:));
    set(gca,'YColor','black');
    hold on;    
end

yyaxis right
plot((slices{1}(:,1)-slices{1}(1,1))/60,slices{1}(:,4),'--','color','red');
hold off;
set(gca,'Ycolor','red');

xlabel('Time (min)')
legend(RelHumidities,'Location','northeast');

yyaxis left

if strcmp('Extinction',Quantity)
    if norm == 1
        title('Y offsets removed')
        ylabel('Extinction shifts (a.u.)');
    else
        title('Y absolute')
        ylabel('Extinctions (a.u.)');
    end
else
    if norm == 1
        title('Y offsets removed')
        ylabel('Centroid position shifts (nm)');
    else
        title('Y absolute')
        ylabel('Centroid positions (nm)');
    end
end

yyaxis right
ylabel('H_2 conc., dashed line (%)')
end

