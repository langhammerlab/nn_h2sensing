function [] = overlying_curves_v4_bas_corr_v3(slices,baselines,RelHumidities,sbp,avg_factor,Quantity)
%% overlying curves
% figure;
subplot(sbp(1),sbp(2),sbp(3));
colors = lines(max(size(slices)));
for i = 1:max(size(slices))
    yyaxis left
    plot((slices{i}(:,1)-slices{i}(1,1))/60,movmean(slices{i}(:,2),avg_factor)-baselines{i}','-','color',colors(i,:));
    set(gca,'YColor','black');
    hold on;    
end

yyaxis right
plot((slices{1}(:,1)-slices{1}(1,1))/60,slices{1}(:,4),'--','color','red');
hold off;
set(gca,'Ycolor','red');

xlabel('Time (min)')
legend(RelHumidities,'Location','northeast');

yyaxis left
title('Y baseline corrected');

if strcmp('Extinction',Quantity)
    ylabel('Extinction shifts (a.u.)');
else
    ylabel('Centroid position shifts (nm)');
end

yyaxis right
ylabel('H_2 conc., dashed line (%)')
end

