function [C,pp,pe] = pcfit_v5(data,lambda,peak_guess,ran,span)
% pcfit_v5: Fits peak's centroid position (version 5). The function also finds peak (max) position and peak extinction. 
% algorithm details: Article below
% - Improving the instrumental resolution of sensors based on localized surface plasmon resonance 
% - DOI: 10.1021/ac0601967 
% 
%       Usage:
%           [C,pp,pe] = pcfit_v5(data,lambda,peak_guess,ran,span)
%
%               data = matrix of spectra ... rows = signal for different wavelengths,
%                                           columns = spectra for different times
%               lambda = wavelength vector
%               peak_guess = guess for peak position
%               ran = +- range around peak_guess to include in a fit    
%               span = span for centroid calculation
%
%               C = centroid vector
%               pp = peak position vector
%               pe = peak extinction vector
%
%       Notes: extinction from 0 to 100.

tic;
warning('off','MATLAB:polyfit:RepeatedPointsOrRescale'); %turns off warnings
    
nr = size(data,2);
range1 = peak_guess-ran;
range2 = peak_guess+ran;
range = [find(range1-lambda<0,1,'first'):find(lambda-range2<0,1,'last')];

C = zeros(1,nr);
pp = zeros(1,nr); % peak position
pe = zeros(1,nr); % peak extinction

n = 20; % fit-polynom degree
lambdanew = lambda(range); % wavelengths included in the fit
m = mean(lambdanew); % quantities for data centering and normalization - accelerates the fitting algorithm (polyfit)
s = std(lambdanew); % details in "help polyfit"
lambdahat = (lambdanew-m)/s;
lambdahat = lambdahat';
left = peak_guess-span/2;
z = 0;

% Peak centroid calculation, algorithm details in DOI: 10.1021/ac0601967 
for i = 1:nr    
    poly = polyfit(lambdahat,data(range,i),n); % coeffs of the polynomial fit
    left = fzero(@(in) polyval(poly,(in-m)/s)-polyval(poly,(in+span-m)/s),left);
    ebase = polyval(poly,(left-m)/s); % algorithm details in DOI: 10.1021/ac0601967 
    C1 = 0;
    C2 = 0;
    for b = 1:n+1
        C1 = C1+poly(b)/(n+3-b)*(((left+span-m)/s)^(n+3-b)-((left-m)/s)^(n+3-b));
        C2 = C2+poly(b)/(n+2-b)*(((left+span-m)/s)^(n+2-b)-((left-m)/s)^(n+2-b));
    end
    C1 = C1-ebase/2*(((left+span-m)/s)^2-((left-m)/s)^2);
    C2 = C2-ebase*(((left+span-m)/s)-(left-m)/s);
    C(i) = C1/C2*s+m;
    
    
    %% Progress bar improvisation - reports progress in the command window
    if (i-z)/nr  >= 0.1
        curt = toc;
        esttot = nr/i*curt; remaining = esttot - curt;
        fprintf('%2.0f%% done, time remaining = %2.1f s\n',i/nr*100, remaining);
        z = i;
    end
    
    %% Peak position (pp) and peak extinction (pe)
    polydata = polyval(poly, lambdahat);
    [pe(i),pp(i)] = max(polydata);
end

pp = lambdanew(pp); %pp = lambdahat(pp)*s+m; % translation of indexes to wavelengths
disp('100% done')



