function [RespMat,RespMatComb,CRespMat,CRespMatComb] = resp_eval_vis_v4(resparr,cresparr,RelHumidities,startsubpl,Quantity)
%   Visualizes the magnitudes of the responses within SSP

    RespMat = reshape(cell2mat(resparr),length(resparr{1}),length(RelHumidities));
    RespMatComb = (RespMat(1:end/2,:) + RespMat(end:-1:end/2+1,:))/2;
    CRespMat = reshape(cell2mat(cresparr),length(resparr{1}),length(RelHumidities));
    CRespMatComb = (CRespMat(1:end/2,:) + CRespMat(end:-1:end/2+1,:))/2;

    colors = copper(length(RelHumidities));

    subplot(1,2,startsubpl);
    plot(CRespMat,RespMat,':x','LineWidth',2,'MarkerSize',7);
    xlabel('{\itc}(H_2) (%)','FontWeight','bold');
    if strcmp('Extinction',Quantity)
        ylabel('Extinction shifts (a.u.)','FontWeight','bold');
    else
        ylabel('Centroid shifts (nm)','FontWeight','bold');
    end
    title('All responses');
    legend(RelHumidities,'Location','best');
    set(gca,'ColorOrder',colors);

    subplot(1,2,startsubpl+1);
    errorbar(CRespMatComb,RespMatComb,abs(RespMat(1:end/2,:)-RespMatComb),':x','LineWidth',2,'MarkerSize',7);
    xlabel('{\itc}(H_2) (%)','FontWeight','bold');
    if strcmp('Extinction',Quantity)
        ylabel('Extinction shifts (a.u.)','FontWeight','bold');
    else
        ylabel('Centroid shifts (nm)','FontWeight','bold');
    end
    title('Corresponding responses coupled');
    legend(RelHumidities,'Location','best');
    set(gca,'ColorOrder',colors);

end

