%% Prepare the workspace
%clear all;
clearvars -except preloaded mypath dtb_content protected
close all;
set(0,'defaultAxesFontSize',12);

%% Open corresponding text files

% Insplorer .inr file
filename = 'pos1_Pd70Au30_A21_pos2_Pd65Au35_A33_Air_Safety_sensor_test_HT_v12';
fileID1 = fopen(strcat('..\Data\43rd experiment SSPv12\Data\',filename,'.inr'),'r');
% LabVIEW txt
fileID2 = fopen(strcat('..\Data\43rd experiment SSPv12\Data\',filename,'.txt'),'r');
% Vaisala (Insight) txt is being opened within a corresponding function 

%% Dataprocessing options
Quantity = 'Centroid';
Mode = 'HT'; % HT mode of the SSPv4
RelHumidities = {'RH 0%','RH 20%','RH 50%','RH 80%','RH 0% Before TR', 'RH 0% After TR'};
RelHumiditiesShort = {'RH 0%','RH 20%','RH 50%','RH 80%','RH 0% BTR', 'RH 0% ATR'};
avg_factor = 15; % number of points averaged for isotherm construction
tautype_value = 0.632; % Defines the type of the time constant, 0.632 for the natural tau, 0.9 for tau90, 0.5 for tau50, etc.
taucalc_mode = 'First'; % Defines the way how the tau is calculated ... first takes the first point corresponding to the tautype_value*100% transition part
tdead = 18; % a time delay between the moment when certain gas mixture starts to be prepared and the moment when it arrives to the sample (with 200 ml/min it is ca 18s at X1)

%% Gas conversion parameters
% MFC parameters are read automatically below ... mfc_param_read
% Ka = 0.689655172413793; % conversion factor from argon to air
% Ra = 500; % range of argon MFC in ml/min
% Kh = 0.901785714285714; % conversion factor from argon to 25% of hydrogen in argon
% Rh = 11.2; % range of hydrogen MFC in ml/min
% xh = 0.25; % 25% H2 in Ar
% Rw = 0.4; % range of water MFC in g/h
rhow = 998.22; % kg/m3 ... water density for 20 C
Mw = 18.01528; % g/mol ... molar mass of water
R = 8.314462618; % J�K-1�mol-1 ... universal gas constant
p = 101325; % Pa
Tw = 20+273.15; % K ... water temperature in Kelvins
Tn = 0+273.15; % K ... normal temperature in Kelvins (from definition)
T = 30+273.15; % K ... reactor temperature in Kelvins
TRH = 29+273.15; % K ... temperature in Kelvins for which the relative humidity level is calculated
tinit = 30; % min ... length of the initialization period before the individual cycle sets
tsd = 10; % min ... length of the desorption period

%% Timestamp autoread from the .inr file
insptime = fgetl(fileID1);
expression = 'Date=';
startIndex = regexp(insptime,expression);
insptime = insptime(startIndex+17:end-8);
Insplorer_t0 = insptime;
fclose(fileID1);

%% Calculate centroid in-situ
peak_guess1 = 710;
ran1 = 150;
span1 = 140;
preloaded{1} = 0;
[tinsp1, cp1, preloaded] = man_fit_v4(filename, peak_guess1, ran1, span1, preloaded); % reading from .ins file

%% Read the data stored in the file from LabVIEW gas management soft
% Read MFC parameters
[Rair,Rarg,Rh1,Rh2,Rw,Kair,Karg,Kh1,Kh2,xh1,xh2] = mfc_param_read_v2(fileID2);

% Read the LabVIEW datafile timestamps
labvtime = fgetl(fileID2);
LabVIEW_t0_long = labvtime(12:end);
LabVIEW_t0_long(end-2) =':'; LabVIEW_t0_long(end-5) =':';
labvtime = labvtime(end-7:end);
labvtime(3) = ':'; labvtime(6) = ':';
LabVIEW_t0 = labvtime;

% Read gas/vapour and reactor state data 
formatSpec = repmat('%f',[1 18]);
SizeF2 = [18 Inf];

labv = fscanf(fileID2,formatSpec,SizeF2);
labv = labv';
tlabv = labv(:,1);
airf = labv(:,10)/100; % airf ... air flow in MFC % 
sairf = labv(:,2)/100; % sairf ... set air flow in MFC %
argf = labv(:,11)/100; % argf ... argon flow in MFC % 
sargf = labv(:,3)/100; % sairf ... set air flow in MFC % 
h1f = labv(:,12)/100;  % hf ... hydrogen flow 1 in MFC %
sh1f = labv(:,4)/100;  % hf ... set hydrogen flow 1 in MFC %
h2f = labv(:,13)/100;  % hf ... hydrogen flow 2 in MFC %
sh2f = labv(:,5)/100;  % hf ... set hydrogen flow 2 in MFC % 
wf = labv(:,14)/100;  % wf ... water flow in MFC %
swf = labv(:,6)/100;  % wf ... set water flow in MFC %
wfm = wf*Rw/60; % wfm ... water flow in g/min
wfV = wfm*R*T/(Mw*p)*10^6; % water flow in ml/min
swfm = swf*Rw/60; % wfm ... set water flow in g/min
swfV = swfm*R*T/(Mw*p)*10^6; % set water flow in ml/min
rtmp = labv(:,18); % ... reactor temperature in deg C
sstmp = labv(:,7); % ... set sample temperature in deg C
sfstmp = labv(:,16); % ... set final sample temperature in deg C (after the ramp up)
stmp = labv(:,15); % ... sample temperature in deg C {... swapped thermocouples ... sample T controlled}

% sfstmp 1st step change suppression ... in some case it happens that the first point measured has the set final sample temperature set to 20 deC, and not to the temperature in the .xlsx script 
sfstmp(1:2) = [sfstmp(3);sfstmp(3)];

% read the data from Vaisala (Insight) txt file
[tvais,mRH,vtmp] = vaisala_reader_v5(filename,LabVIEW_t0_long); % mRH ... measured humidity, usually in the form of absolute humidity at NTP

%% Process values calculation
pwsat = 1e3*exp(16.2886-3816.44/(T+(-46.13))); % saturated vapor pressure in Pa
RH = (wfV./(h1f*Rh1*Kh1+h2f*Rh2*Kh2+airf*Rair*Kair+argf*Rarg*Karg+wfV))/(pwsat/p)*100; % relative humidity in %
sRH = (swfV./(sh1f*Rh1*Kh1+sh2f*Rh2*Kh2+sairf*Rair*Kair+sargf*Rarg*Karg+swfV))/(pwsat/p)*100; % set relative humidity in %

tv = (xh1*h1f*Rh1*Kh1+xh2*h2f*Rh2*Kh2)./(h1f*Rh1*Kh1+h2f*Rh2*Kh2+airf*Rair*Kair+argf*Rarg*Karg+wfV)*100; % hydrogen concentration = target value
stv = (xh1*sh1f*Rh1*Kh1+xh2*sh2f*Rh2*Kh2)./(sh1f*Rh1*Kh1+sh2f*Rh2*Kh2+sairf*Rair*Kair+sargf*Rarg*Karg+swfV)*100; % set hydrogen concentration = set target value

%% TR step manual labelling
% This labelling expects tsampling in LabVIEW data to be equal to 1 s
if strcmp('HT',Mode)
    sfstmp(601:22200) = 80.1;
    sfstmp(83417:105016) = 80.1;
end

%% Sync the tracks
totshift = tlabv(1);
[tlabv, tinsp1, cp1, cpi1] = sync_v3(tlabv, tinsp1, cp1, LabVIEW_t0, Insplorer_t0, tdead);
totshift = tlabv(1) - totshift;
tvais = tvais + totshift; % synchronization of Vaisala time vector with tlabv
%% mRH supersampling
[tlabv, tvais, mRH, mRHi] = sync_v3(tlabv, tvais, mRH, LabVIEW_t0, LabVIEW_t0, 0);

%% recalculation of absolute humidity at NTP to relative humidity at sample temperature (or chosen static temperature)
% Arden Buck equation, more accurate, https://en.wikipedia.org/wiki/Vapour_pressure_of_water
TRHC = TRH - 273.15; % chosen temperature for RH calculation in �C
pwsat_vec = 6.1121*exp((18.678-TRHC/234.5)*(TRHC/(257.14+TRHC)))*100; % saturated vapor pressure in Pa
% absolute humidity at NTP (g/m3) recalculation to water vapour partial pressure (Pa)
% ideal gas approx., consistent with product's manual, https://www.manualsdir.com/manuals/408481/vaisala-dm500.html?page=133
pw_vec = mRHi*R*Tn/Mw; % measured water vap. pressure in Pa, R*Tn/Mw = 273.15/2.16679 
mRHi = pw_vec./pwsat_vec*100; % now mRHi contains relative humidity at 30 �C (or another selected static temperature)

%% Visualize
totf = h1f*Rh1*Kh1+h2f*Rh2*Kh2+airf*Rair*Kair+argf*Rarg*Karg+wfV; % Total flow
stotf = sh1f*Rh1*Kh1+sh2f*Rh2*Kh2+sairf*Rair*Kair+sargf*Rarg*Karg+swfV; % Set total flow
xlim = [0 2400];
fh{1} = visualisedt_v15(tinsp1,cp1,tlabv,tv,mRHi,totf,stmp,xlim,Quantity);

ax1 = fh{1}.Children(3); axes(ax1);
set(gca,'YLim', [0 1.5]);
yyaxis left

%% Separation of individual cycle sets
slices1 = slicer_v5(sRH, sfstmp, tlabv, cpi1, tv, stv, tinit, tsd);

%% Visualisations of the slices
% Overlay visualisation of the slices
fh{2} = figure;
sbp = [2,1,1];
overlying_curves_v6(slices1,RelHumidities,sbp,0,avg_factor,Quantity);
sbp = [2,1,2];
overlying_curves_v6(slices1,RelHumidities,sbp,1,avg_factor,Quantity);

% Baseline subtracted graphs - for convenient visual inspection 
baselines1 = baseline_creator_v3(slices1,avg_factor);

fh{3} = figure;
sbp = [1,1,1];
overlying_curves_v4_bas_corr_v3(slices1,baselines1,RelHumidities,sbp,avg_factor,Quantity);

% Stacked graphs
set(0,'defaultAxesFontSize',10);
fh{4} = figure;
uYax = [1,1,1.5]; % uniform YLim parameters, [1 .. slice number where to take the YLim from, 2 .. nm to be added below the min, 3 .. nm to be added above the max] 
stacked_graphs_bas_corr_v6(slices1,baselines1,RelHumidities,avg_factor,uYax,Quantity)

%% Automatic response evaluation
[resparr1,cresparr1,tauarr1,atauarr1,dtauarr1] = resp_eval_v4(slices1,avg_factor,tautype_value,taucalc_mode);

fh{5} = figure;
[RespMat1,RespMat1Comb,CRespMat1,CRespMat1Comb] = resp_eval_vis_v4(resparr1,cresparr1,RelHumidities,1,Quantity);

% Comparison of the responses towards the highest concentration - ISO compliance
CSel = 5; % Selection of the concentration step (step nr.) for which the ISO compliance will be evaluated
fh{6} = figure('units','normalized','position',[0.3 0.3 0.38 0.39]);
iso_compl_v5(RespMat1Comb,Quantity,CSel,RelHumiditiesShort);

%% Close the files used
fclose('all');

%% LoD calculation
LoDMode = 'linlog10';
posorneg = 1;
temperature = 80;

[prt1,LoD1,stdm1,stdmc1] = prtrf_v11(RespMat1Comb,CRespMat1Comb,slices1,tinsp1,cp1,LoDMode,posorneg);

M = [prt1',LoD1',stdm1'];

%% LoD visualisation
nfiles = 1;
RHl = [0, 20, 50, 80]; % relative humidities in the core section of the SSPv12 protocol

prt(:,nfiles) = M(:,1);
LoD(:,nfiles) = M(:,2);
stdm(:,nfiles) = M(:,3);
stdmc(:,:,nfiles) = stdmc1;
RespMats{nfiles} = {CRespMat1,CRespMat1Comb,RespMat1,RespMat1Comb};
resptimes{nfiles} = {tauarr1,atauarr1,dtauarr1};

clear stdm1
%% response magnitudes extraction from the database
for i = 1:length(RHl) % extract all humidities, control section checks excluded 
    for j = 1:nfiles 
        aresp(:,i,j) = RespMats{j}{4}(:,i); % response matrix (combined) for temp{j} and RH(i) %
        aresp_all(:,i,j) = RespMats{j}{3}(:,i); % response matrix (all individual transitions) for temp{j} and RH(i) %
        cresp(:,i,j) = RespMats{j}{2}(:,i); % concentration matrix for temp{j} and RH(i) %
        stdm1(:,i,j) = repmat(stdm(i,j),[size(aresp,1),1]);
    end    
end

%% Error estimation
aux = aresp_all(1:end/2,:,:); aux(:,:,:,2) = aresp_all(end:-1:end/2+1,:,:);
Er = std(aux,0,4); % standard deviation coming from the pulse repetition
E = sqrt(Er.^2+stdm1.^2+stdmc(:,1:length(RHl),:).^2); % combined standard deviation (pulse repetition + the signal noise component)

%% Visualization
colors = copper(nfiles);
 
f1 = figure;
j = 1;
hl = errorbar(cresp(:,4,j),aresp(:,4,j),3*E(:,4,j),'--o','MarkerFaceColor','w','LineWidth',1); hold on;

% Decorations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear p
for i = 4:4
    hl(1).Color = colors(1,:);
    p{i} = myshadow_v16(cresp(:,i,j),[aresp(:,i,j), 3*E(:,i,j)],1,'color',hl(1).Color,'distribution','linearerror','intensity',0.2);
    uistack(p{i},'bottom');
end

set(gca, 'LineWidth', 1.0, 'XScale', 'log', 'XTick', [0.1, 0.3, 1], 'XTickLabel', [{'10^{-1}'},{'3*10^{-1}'},{'10^{0}'}], ...
    'XLim',[0.05, 1.5], 'YLim' ,[-0.8 2.2], 'FontSize', 11);

Tleg = [repmat('RH ',[length(RHl),1]), num2str(RHl'), repmat('%',[length(RHl),1])];

for i = 1:length(hl)
    hl(i).DisplayName = Tleg(i,:);
end

hl2 = plot([get(gca,'XLim')],[0 0],'-r','LineWidth',1.0);
uistack(hl2,'bottom');
hl2 = plot([prt(4,j),prt(4,j)],[get(gca,'YLim')],':r','LineWidth',1.0);
uistack(hl2,'bottom');

hl2 = plot([get(gca,'XLim')],[3*stdm(4,j) 3*stdm(4,j)],'-b','LineWidth',1.0);
uistack(hl2,'bottom');
hl2 = plot([LoD(4,j),LoD(4,j)],[get(gca,'YLim')],':b','LineWidth',1.0);
uistack(hl2,'bottom');

legend('Limit of detection','\Delta{\it\lambda}_{peak} = 3\sigma', ...
    'c(\Delta{\it\lambda}_{peak} = 0 nm) %','\Delta{\it\lambda}_{peak} = 0 nm','Location','eastoutside')
hold off;

ylabel('\Delta{\it\lambda}_{peak} (nm)','FontWeight','bold', 'FontSize', 12) % left y-axis
xlabel('{\itc}_H_2 (%)','FontWeight','bold', 'FontSize', 12)








