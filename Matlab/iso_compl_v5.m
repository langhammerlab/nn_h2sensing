function [] = iso_compl_v5(RespMatComb,Quantity,CSel,RelHumidities)
% Comparison of the responses towards the highest concentration - ISO compliance
% 
    area([0,7],[RespMatComb(CSel,3)*1.3 RespMatComb(CSel,3)*1.3], RespMatComb(CSel,3)*0.7,'FaceColor','r','FaceAlpha',0.3, ...
        'LineStyle', 'none'); % ISO boundaries box
    hold on;
    plot(RespMatComb(CSel,:),':xb','LineWidth',2,'MarkerSize',7);
    aux = gca;
    set(gca,'XTickLabel',[{' '},RelHumidities,{' '}]')
    xlabel('Relative humidity (RH), BTR = Before Temperature Regeneration, ATR = After TR','FontWeight','bold');
    if strcmp('Extinction',Quantity)
        ylabel('Extinction shifts (a.u.)','FontWeight','bold');
    else
        ylabel('Centroid shifts (nm)','FontWeight','bold');
    end
    title('ISO compliance test');
    mYLim = get(gca,'YLim');
    if mYLim(1) > 0
        set(gca,'YLim',[0 mYLim(2)]);
    end
    if mYLim(2) < 0 && mYLim(1) < 0
        set(gca,'YLim',[mYLim(1) 0]);
    end

    % ISO boundaries +- 30%
    plot([0,7],[RespMatComb(CSel,3)*1.3 RespMatComb(CSel,3)*1.3],':r','LineWidth',2);
    plot([0,7],[RespMatComb(CSel,3)*0.7 RespMatComb(CSel,3)*0.7],':r','LineWidth',2);
    plot([2,4],[RespMatComb(CSel,2), RespMatComb(CSel,4)],'xm','MarkerSize',9,'LineWidth',2);

    txt = 'ISO compliant';
    text(0.5,RespMatComb(CSel,3),txt);
    txt = 'ISO incompliant';
    if RespMatComb(CSel,3) < 0
        text(0.5,mYLim(2)/2,txt); % Check this here
    else
        text(0.5,RespMatComb(CSel,3)*0.6,txt);
    end
    txt = 'The magenta crosses should lie inside the red ISO compliance box';
    text(1,0.1*mYLim(2),txt);

    hold off;
end

