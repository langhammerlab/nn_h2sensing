function [resparr,cresparr,tauarr,atauarr,dtauarr] = resp_eval_v4(slices,avg_factor,tautype_value,taucalc_mode)
% Description - Automatic evaluation of responses

for i = 1:length(slices)
    D  = diff(slices{i}(:,4));
    CV = abs([D ; 0]) >= 0.01; % change vector, marks the position where the changes in hydrogen concentration occur (marks the last point before the change)
    k = 1; lj = 1;
    
    for j = 1:length(CV)
        if CV(j) ~= 0
            % glitch detection ... 5 s dead/tolerance time
            if slices{i}(j,1) - slices{i}(lj,1) > 5 
                %% response magnitudes calculation 
                SP{i}(k) = mean(slices{i}(j-avg_factor+1:j,2)); % SP ... significant points ... either the beginning or the end of the response
                CSP{i}(k) = mean(slices{i}(j-avg_factor+1:j,3)); % concentration significant points
                
                %% response times calculation
                if k >= 2
                    if strcmpi(taucalc_mode,'closest')
                        AR = abs(SP{i}(k)-SP{i}(k-1)); % absolute response
                        dv = abs(movmean(slices{i}(lj:j,2),avg_factor)-mean(slices{i}(lj-avg_factor+1:lj,2)))-tautype_value*AR; % difference vector
                        D2 = abs(dv);
                        [~, smdp] = min(D2);
                        tauarr{i}(k-1) = slices{i}(lj+smdp-1,1) - slices{i}(lj,1) - (slices{i}(lj+1,1) - slices{i}(lj,1))/2; % smdp-1 because MATLAB is indexing from 1, corrected because lj is the last point before the change                       
                    else
                        R = SP{i}(k)-SP{i}(k-1); % response
                        dv = movmean(slices{i}(lj:j,2),avg_factor)-mean(slices{i}(lj-avg_factor+1:lj,2))-tautype_value*R; % difference vector
                        pdv = dv*sign(R); % converted to positive difference scenario
                        if strcmpi(taucalc_mode,'first')
                            scp = find(pdv > 0, 1); % sign change position
                        else % strcmpi(taucalc_mode,'last')
                            scp = find(pdv < 0, 1, 'last'); % sign change position
                            scp = scp + 1; % to correspond to the last "first greater than 0 point" so that it can share the following assignment
                        end
                        if ~isempty(scp)
                            tauarr{i}(k-1) = slices{i}(lj+scp-1,1) - slices{i}(lj,1) - (slices{i}(lj+1,1) - slices{i}(lj,1)); % scp-1 because MATLAB is indexing from 1, corrected because lj is the last point before the change
                        else
                            tauarr{i}(k-1) = Inf; % corresponding tau was not found
                        end
                    end
                end
                %%                
                k = k + 1;
                lj = j;
            end
        end
            
    end
    %% Last tau calculation
    if strcmpi(taucalc_mode,'closest')
        AR = abs(mean(slices{i}(end-avg_factor+1:end,2))-SP{i}(k-1)); % absolute response
        dv = abs(movmean(slices{i}(lj:end,2),avg_factor)-mean(slices{i}(lj-avg_factor+1:lj,2)))-tautype_value*AR; % difference vector
        D2 = abs(dv);
        [~, smdp] = min(D2);
        tauarr{i}(k-1) = slices{i}(lj+smdp-1,1) - slices{i}(lj,1) - (slices{i}(lj+1,1) - slices{i}(lj,1))/2; % smdp-1 because MATLAB is indexing from 1, corrected because lj is the last point before the change
    else
        R = mean(slices{i}(end-avg_factor+1:end,2))-SP{i}(k-1); % response
        dv = movmean(slices{i}(lj:end,2),avg_factor)-mean(slices{i}(lj-avg_factor+1:lj,2))-tautype_value*R; % difference vector
        pdv = dv*sign(R); % converted to positive difference scenario
        if strcmpi(taucalc_mode,'first')
            scp = find(pdv > 0, 1); % sign change position
        else % strcmpi(taucalc_mode,'last')
            scp = find(pdv < 0, 1, 'last'); % sign change position
            scp = scp + 1; % to correspond to the last "first greater than 0 point" so that it can share the following assignment
        end
        if ~isempty(scp)
            tauarr{i}(k-1) = slices{i}(lj+scp-1,1) - slices{i}(lj,1) - (slices{i}(lj+1,1) - slices{i}(lj,1)); % scp-1 because MATLAB is indexing from 1, corrected because lj is the last point before the change
        else
            tauarr{i}(k-1) = Inf; % corresponding tau was not found
        end        
    end
        
    %% calculation of responses
    resparr{i} = SP{i}(2:2:end) - SP{i}(1:2:end);   % responses in nm
    cresparr{i} = CSP{i}(2:2:end); % corresponding H2 concentrations in %
    atauarr{i} = tauarr{i}(1:2:end); dtauarr{i} = tauarr{i}(2:2:end); % separated absorption and desorption times
       
end

end