function [tlabv, tinsp, cp, cpi] = sync_v3(tlabv, tinsp, cp, LabVIEW_t0, Insplorer_t0, arbshift)
% %% Sync the tracks, faster version of v1
% % Shift tlabv

[Y, M, D, H, MN, S] = datevec(LabVIEW_t0);
LWt0 = H*3600+MN*60+S;
[Y, M, D, H, MN, S] = datevec(Insplorer_t0);
Inspt0 = H*3600+MN*60+S;
Shift = LWt0 - Inspt0; 

tlabv = tlabv + Shift;
tlabv = tlabv + arbshift;

% Sync
tinsp(end+1) = max([tinsp(end); tlabv(end)])+1; tinsp(end+1) = max([tinsp(end); tlabv(end)])+2;
cp(end+1) = cp(end); cp(end+1) = cp(end);
cpi(1) = cp(1); cpi(2) = cp(1);

j = 1;
for i = 3:length(tlabv)    
    while tlabv(i) > tinsp(j)
       j = j + 1;       
    end
    if tlabv(i) <= tinsp(1)
       cpi(i) = cp(1);
    else
        cpi(i) = (tlabv(i)-tinsp(j-1))/(tinsp(j)-tinsp(j-1))*cp(j)+(tinsp(j)-tlabv(i))/(tinsp(j)-tinsp(j-1))*cp(j-1);
    end
end

cpi = cpi';

end