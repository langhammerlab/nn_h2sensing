function [prt,LoD,stdm,stdmc] = prtrf_v11(RespMatComb,CRespMatComb,slices,tinsp,cp,interpmode,posorneg)
% Positive response threshold - reading function

%% Data variance
% tinsp data portion for calcuation

npoints = 60; % 3 min interval with 3s sampling
npointsc = 15; % 45 s interval with 3s sampling for the variance estimation at nonzero H2 concentrations

if posorneg == -1 % pos or neg determines whether we are looking for positive or negative responses (positive ... posorneg == 1, negative ... posorneg == -1)
    RespMatComb = -RespMatComb;
elseif posorneg == 0 % looking for absolute responses no matter wheter positive or negative
    RespMatCombBackup = RespMatComb;
    RespMatComb = abs(RespMatComb);
end

for i = 1:length(slices)

    D  = diff(slices{i}(:,4));
    CV = abs([D ; 0]) >= 0.01; % change vector, marks the position where the changes in hydrogen concentration occur (marks the last point before the change)
    k = 1; j = 1;
    
    while k == 1 % finds the last j before the 1st response
        if CV(j) ~= 0
            k = k + 1;
        end
        j = j + 1;
    end
    lj = j - 1;
    
    tbfe = slices{i}(lj,1); % time before the 1st H2 exposure
    
    D2 = abs(tinsp-tbfe);
    [~, smdp] = min(D2);
    
    if tinsp(smdp) > tbfe
        smdp = smdp - 1;
    end
    
    stdm(i) = std(cp(smdp-npoints+1:smdp));
    
    %% calculation for all cocentrations
    
    k = 1;
    m = 1;
    lj = 1;
    
    for j = 1:length(CV) % finds the last j before the 1st response
        if CV(j) ~= 0 && slices{i}(j,1) - slices{i}(lj,1) > 5  % glitch detection ... 5 s dead/tolerance time
            if rem(k, 2) == 0 % only desorption(recovery) edge 
                tbfe = slices{i}(j,1); % time before the 1st recovery point from H2 exposure
                
                D2 = abs(tinsp-tbfe);
                [~, smdp] = min(D2);
                
                if tinsp(smdp) > tbfe
                    smdp = smdp - 1;
                end
                
                stdmA(m) = std(cp(smdp-npointsc+1:smdp));
                m = m + 1;
                lj = j;
            end
            k = k + 1;
        end
        
    end
    
    stdmc(:,i) = (stdmA(1:end/2) + stdmA(end:-1:end/2+1))/2;
     
    
    %% Threshold localization (LoD)
    
    %% find 1st m with response greater than 3sigma
    srmc = size(RespMatComb);
    
    m = 1;
    stopper = 0; LoDfound = 0;
       
    while stopper == 0
        if RespMatComb(m,i) > 3*stdm(i)  % if the LoD is close to the highest concentration, consider using >=
            stopper = 1;
            LoDfound = 1;
            m = m - 1;
        end
        m = m + 1;
        if m > srmc(1)
            stopper = 1;
        end
    end
    
    %% calculate prt and LoD
    if LoDfound
        % Response matrix recovery if absolute values are used (... double sensor approach)
        if posorneg == 0
            if RespMatCombBackup(m,i) > 0
                RespMatComb(:,i) = RespMatCombBackup(:,i);
            else
                RespMatComb(:,i) = -RespMatCombBackup(:,i);
            end
        end
        %
        if m == 1
            switch lower(interpmode)
                case {'lin','linear','linlog10'}
                    disp('Lin interpolation to 0% H2 concentration used');
                    prt(i) = 0;
                    LoD(i) = 3*stdm(i)/RespMatComb(m,i)*CRespMatComb(m,i);
                case {'log','logarithmic','log10'}
                    disp('Log10 interpolation not possible (... 0% H2 concentration), log10 extrapolation used');
                    prt(i) = CRespMatComb(m,i)*10^(-RespMatComb(m,i)*(log10(CRespMatComb(m+1,i))-log10(CRespMatComb(m,i)))/(RespMatComb(m+1,i)-RespMatComb(m,i)));
                    LoD(i) = CRespMatComb(m,i)*10^(-(RespMatComb(m,i)-3*stdm(i))*(log10(CRespMatComb(m+1,i))-log10(CRespMatComb(m,i)))/(RespMatComb(m+1,i)-RespMatComb(m,i)));
            end
        else
            switch lower(interpmode)
                case {'lin','linear'}
                    prt(i) = (0-RespMatComb(m-1,i))/(RespMatComb(m,i)-RespMatComb(m-1,i))*(CRespMatComb(m,i)-CRespMatComb(m-1,i))+CRespMatComb(m-1,i);
                    LoD(i) = (3*stdm(i)-RespMatComb(m-1,i))/(RespMatComb(m,i)-RespMatComb(m-1,i))*(CRespMatComb(m,i)-CRespMatComb(m-1,i))+CRespMatComb(m-1,i);
                case {'log','logarithmic','log10','linlog10'}
                    prt(i) = 10^((0-RespMatComb(m-1,i))/(RespMatComb(m,i)-RespMatComb(m-1,i))*(log10(CRespMatComb(m,i))-log10(CRespMatComb(m-1,i)))+log10(CRespMatComb(m-1,i)));
                    LoD(i) = 10^((3*stdm(i)-RespMatComb(m-1,i))/(RespMatComb(m,i)-RespMatComb(m-1,i))*(log10(CRespMatComb(m,i))-log10(CRespMatComb(m-1,i)))+log10(CRespMatComb(m-1,i)));
            end
        end
    else
        prt(i) = Inf;
        LoD(i) = Inf;
    end

end







