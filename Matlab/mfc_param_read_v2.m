function [Rair,Rarg,Rh1,Rh2,Rw,Kair,Karg,Kh1,Kh2,xh1,xh2] = mfc_param_read_v2(fileID)
% Reads MFC parameters from LabVIEW log file header
% reads ranges, k values and hydrogen concentration

% skip the header
header = 5;

for k=1:header
    fgetl(fileID);
end

fscanf(fileID,'%s',2); % reads the words before the numerical values

Rair = fscanf(fileID,'%f',1); % range of air MFC in ml/min
Rarg = fscanf(fileID,'%f',1); % range of argon MFC in ml/min
Rh1 = fscanf(fileID,'%f',1); % hydrogen stream 1 MFC, ml/min
Rh2 = fscanf(fileID,'%f',1); % hydrogen stream 2 MFC, ml/min
Rw = fscanf(fileID,'%f',1); % liquid water MFC, g/h

fgetl(fileID);

fscanf(fileID,'%s',1); % reads the words before the numerical values

Kair = fscanf(fileID,'%f',1); % conversion factor from argon to air
Karg = fscanf(fileID,'%f',1); % conversion factor from argon to argon
Kh1 = fscanf(fileID,'%f',1); % conversion factor from argon to 25% of hydrogen in argon
Kh2 = fscanf(fileID,'%f',1); % conversion factor from argon to 25% of hydrogen in argon


fgetl(fileID); fgetl(fileID);

fscanf(fileID,'%s',4);

xh1 = fscanf(fileID,'%f',1)/100; % 25% H2 in Ar
xh2 = fscanf(fileID,'%f',1)/100; % 25% H2 in Ar

header = 7;

for k=1:header
     fgetl(fileID);
end

end