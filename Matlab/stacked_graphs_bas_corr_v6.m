function [] = stacked_graphs_bas_corr_v6(slices,baselines,RelHumidities,avg_factor,uYax,Quantity)
% displays the slices in stacked graphs 
% figure;

set(0,'defaultAxesFontSize',12);
t = tiledlayout(length(slices),1);
colors = copper(length(slices));

for i = 1:length(slices)
    ax{i} = nexttile;
    yyaxis left
    plot(ax{i},(slices{i}(:,1)-slices{i}(1,1))/60,movmean(slices{i}(:,2),avg_factor)-baselines{i}','-','color',colors(i,:));
    set(gca,'YColor','black');
    
    if i == uYax(1) % save the Y axis limits of the reference graph
       muYLim = get(gca,'ylim');       
    end
    
    if i ~= length(slices) % removes the x axis ticks for all graphs except for the bottom one
       xticklabels(ax{i},{});
    end
    
    yyaxis right
    plot(ax{i},(slices{1}(:,1)-slices{1}(1,1))/60,slices{1}(:,4),'--','color','red');
    set(gca,'Ycolor','red');
    
    if i == ceil(length(slices)/2) % removes the x axis ticks for all graphs except for the bottom one
       ylabel('{\itc}(H_2) (%)')
    end  
end

muXLim = get(gca,'xlim');

% Graph labelling
title(t,'Y baseline corrected');

if strcmp('Extinction',Quantity)
    ylabel(t,'Extinction shifts (a.u.)');
else
    ylabel(t,'Centroid position shifts (nm)');
end
xlabel(t,'Time (min)')

t.TileSpacing = 'none';
t.Padding = 'compact';

%% Uniform YLim rescaling
if uYax(1) > 0
    for i = 1:length(slices)
        set(gcf, 'currentaxes', ax{i});
        yyaxis left
        set(gca,'YLim', [muYLim(1)-uYax(2),muYLim(2)+uYax(3)]);
        text(muXLim(2)*0.05,muYLim(2)*0.7,RelHumidities{i});
    end
end

set(0,'defaultAxesFontSize',10);
end

