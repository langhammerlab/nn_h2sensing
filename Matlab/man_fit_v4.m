function [t1, C1, preloaded] = man_fit_v4(filename, peak_guess1, ran1, span1, preloaded)
start = datetime;
tic;
%% Data reading
if ~preloaded{1}
    [t1, ~, wl, spectra1, ~] = InsRead2DT_v6(strcat('..\Data\43rd experiment SSPv12\Data\',filename,'.ins'));
else
    t1 = preloaded{2};
    wl = preloaded{3};
    spectra1 = preloaded{4};
end
toc;

%% Centroid calculation
% Original method ... PeakFitLorentzCentroid
[C1,~,~] = pcfit_v5(spectra1',wl,peak_guess1,ran1,span1); toc;
          
finish = datetime;
mduration = finish - start;

preloaded{1} = 1;
preloaded{2} = t1;
preloaded{3} = wl;
preloaded{4} = spectra1;





