function [t1, t2, wl, spectra1, spectra2] = InsRead2DT_v6(file)
% [t wl spectra1 spectra2] = InsRead2('file.ins') 
% Reads a 2 channel ins file 
% t = time vector in sec
% wl = wavelength vector
% spectra1 and spectra2 = transmission spectra for the two channels

ID = fopen(file);

for i = 1:3
    a = fgetl(ID);
end
wl = str2num(a);

for i = 1:2
    a = fgetl(ID);
end
a(1:4) = [];
dark1 = str2num(a);

a = fgetl(ID);
a(1:4) = [];
dark2 = str2num(a);

for i = 1:2
    a = fgetl(ID);
end
a(1:4) = [];
ref1 = str2num(a);

a = fgetl(ID);
a(1:4) = [];
ref2 = str2num(a);

fgetl(ID);

%% Aproximate preallocation
s = dir(file);
bs = s.bytes;
margin = 0.1; % vectors and matrices are preallocated a bit greater
nr_est = ceil(bs/157908928*14110); % estimated number of lines
nr_ovest = ceil(nr_est*(1+margin)); % overestimated number of lines

spectra1 = zeros(ceil(nr_ovest/2),length(wl));
spectra2 = zeros(ceil(nr_ovest/2),length(wl));
t1 = zeros(1,ceil(nr_ovest/2));
t2 = zeros(1,ceil(nr_ovest/2));
c1 = 1; c2 = 1;

%% Data reading
nr = 1;
z = 0;

while ischar(a)
    a = fgetl(ID);
    
    if ischar(a)
        if strcmp(a(1:3),'ch1')
            a(1:4) = [];
            b = str2num(a);
            t1(c1) = b(1);
            b(1) = [];
            spectra1(c1,:) = (1-(b-dark1)./(ref1-dark1))*100;
            c1 = c1 + 1;
        else
            a(1:4) = [];
            b = str2num(a);
            t2(c2) = b(1);
            b(1) = [];
            spectra2(c2,:) = (1-(b-dark2)./(ref2-dark2))*100;
            c2 = c2 + 1;
        end
        
    end
    
    %% Progress bar improvisation
    if (nr-z)/nr_est  >= 0.01
        curt = toc;
        esttot = nr_est/nr*curt; remaining = esttot - curt;
        fprintf('%2.0f%% done, time remaining = %2.1f s\n',nr/nr_est*100, remaining);
        z = nr;
    end 
    
    nr = nr+1;
end
fclose(ID);

spectra1(c1:end,:) = [];
spectra2(c2:end,:) = [];
t1(c1:end) = [];
t2(c2:end) = [];

disp('100 % done')

end

