function varargout = myshadow_v16(x,f,active,varargin)
% MYSHADOW_V15 Draws a shadow around a curve
%   MYSHADOW_V15(X,Y,1) draws a shadow below the curve represented by vectors
%   X and Y. The 3rd argument activates (==1) or deactivates (==0) drawing of the
%   shadow.
%   MYSHADOW_V15(...,Name,Value) creates the shadow with one or more
%   properties specified using name-value pair arguments. You can specify
%   the following properties:
%       'orientation' ... shadow position: 'up','down','both',
%                           'inbetween' ... in this case it fills the area
%                           in-between 2 curves specified in 2-column Y
%                           matrix [Y1,Y2]
%       'distribution' ... shadow fall off:
%                           'linear'
%                           'linearplus' ... the shadow vanishes linearly
%                           down to min(f)-thickness (or max(f)+thickness)
%                           'linearerror' ... the shadow represents
%                           continuous error bars (~ confidence band)
%                               in this case the Y is anticipated as
%                               2-column matrix: [Ydata,Yerror]
%                           'hyperbolic',
%                           'gaussian',
%                           'summarizing' ... resembles Fibonacci series
%       'color' ... shadow color: 'r' or [x,y,z]/255
%       'thickness' ... shadow thickness in corresponding Y units (or normalized to YLim span, range of 0 - 1,
%                       requires 'thicknessunits' set to 'normalized')
%       'thicknessunits' ... 'absolute' (default), 'normalized'
%       'nsteps' ... quality of the shadow (in how many steps is drawn), default = 10
%                    (linear shadows do not require this parameter, in their case it is Infinite)
%       'intensity' ... maximum intensity of the shadow (... FaceAlpha), in the range 0 - 1
%       'sharpness' ... sharpness of the Gaussian peak, default = 0.1
%                    (lower values provide sharper shapes)
%
% Example
%     x = 0:0.01:10;
%     y = sin(x);
% 
%     subplot(2,2,1);
%     plot(x,y);
%     myshadow_v15(x,y,1);
% 
%     subplot(2,2,2);
%     plot(x,y);
%     myshadow_v15(x,y,1,'distribution','linearplus','orientation','both');
% 
%     subplot(2,2,3);
%     plot(x,y);
%     myshadow_v15(x,y,1,'distribution','gaussian','color','red');
% 
%     subplot(2,2,4)
%     y2 = 0.2*x-1;
%     plot(x,y,'b',x,y2,'g');
%     z = sin(3*x)/2+1;
%     myshadow_v15(x,[y',z'],1,'distribution','linearerror','color','red','intensity',0.5);



if active
    
    %% Default values
    up = 0;
    down = 1;
    distribution = 'linear';
    mcolor = 'k';
    %thickness = 5; % The default value is 10 % of corresponding YLim span
    nsteps = 10;
    intensity = 0.3;
    sharpness = 0.1;
    
    %% Was and ev. how was the thickness specified?
    normthickness = 0;
    specthickness = 0;
    
    %% Auxiliary variables
    handlecounter = 1;
    
    %% Modifiers, made case insensitive
    for i = 1:length(varargin)/2
        
        switch lower(varargin{2*i-1})
            case {'orientation','position','shadowposition'}
                switch lower(varargin{2*i})
                    case 'up'
                        up = 1;
                        down = 0;
                    case 'down'
                        up = 0;
                        down = 1;
                    case 'both'
                        up = 1;
                        down = 1;
                    case 'inbetween'
                        up = 0;
                        down = 0;
                end
            case 'distribution'
                distribution = varargin{2*i};
            case 'color'
                mcolor = varargin{2*i};
            case 'thickness'
                thickness = varargin{2*i};
                specthickness = 1;
            case {'nsteps','numberofsteps','numsteps','steps'}
                nsteps = varargin{2*i};
            case 'intensity'
                intensity = varargin{2*i};
            case {'sharpness','shadowsharpness'}
                sharpness = varargin{2*i};
            case {'thicknessunits','thicknessunit','units','unit'}
                switch lower(varargin{2*i})
                    case {'absolute','abs'} % Default case
                        
                    case {'normalized','norm'}  %% Normalized thickness - norm detection
                        g = groot;
                        normthickness = 1;
                        if ~isempty(g.Children) % ~(True if there are no open graphics objects, false otherwise)
                            ylim = get(gca,'YLim');
                            dylim = ylim(2) - ylim(1);
                        else
                            dylim = max(f) - min(f); % approximate ylimits
                        end
                end
                
        end
        
        
    end
    
    
    if ~specthickness
        g = groot;
        if ~isempty(g.Children) % ~(True if there are no open graphics objects, false otherwise)
            ylim = get(gca,'YLim');
            dylim = ylim(2) - ylim(1);
        else
            dylim = max(f) - min(f); % approximate ylimits
        end
        thickness = dylim*0.1;
    else
        if normthickness
            thickness = dylim*thickness;
        end
    end
    
    %% Shadow/decoration drawing

    % Define the vertices: the points at (x, f(x)) and (x, 0)
    N = length(x);
    % Define the faces to connect each adjacent f(x) and the corresponding points at y = 0.
    q = (1:N-1)';

    for j = 1:(up+down)
        if up
            sgn = 1;
            if up&&down&&(j==2)
                sgn = -1;
            end
        else
            sgn = -1;
        end
        
        switch lower(distribution)
            case 'linear'
                faces = [q, q+1, q+N+1, q+N];
                verts = [x(:), f(:); x(:), f(:)+sgn*thickness];
                intstep = [ones(N,1)*intensity;zeros(N,1)];
                if exist('p','var')
                   handlecounter = handlecounter + 1; 
                end
                p(handlecounter) = patch('Faces', faces, 'Vertices', verts, 'FaceColor', mcolor, 'EdgeColor', 'none', 'FaceAlpha', 'interp', 'AlphaDataMapping', 'none', 'FaceVertexAlphaData', intstep);
                set(get(get(p(handlecounter),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
                
            case 'linearplus'
                faces = [q, q+1, q+N+1, q+N];
                if sgn == 1
                    verts = [x(:), f(:); x(:), ones(N,1)*(max(f)+sgn*thickness)];
                    intstep = [ones(N,1)*intensity.*(max(f)+sgn*thickness-f')/(max(f)+sgn*thickness-min(f));zeros(N,1)];
                else % sgn == -1
                    verts = [x(:), f(:); x(:), ones(N,1)*(min(f)+sgn*thickness)];
                    intstep = [ones(N,1)*intensity.*(f'-(min(f)+sgn*thickness))/(max(f)-(min(f)+sgn*thickness));zeros(N,1)];
                end
                if exist('p','var')
                   handlecounter = handlecounter + 1; 
                end
                p(handlecounter) = patch('Faces', faces, 'Vertices', verts, 'FaceColor', mcolor, 'EdgeColor', 'none', 'FaceAlpha', 'interp', 'AlphaDataMapping', 'none', 'FaceVertexAlphaData', intstep);
                set(get(get(p(handlecounter),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
                
            case 'linearerror'
                faces = [q, q+1, q+N+1, q+N];
                verts = [x(:), f(:,1); x(:), f(:,1)+f(:,2)];
                intstep = [ones(N,1)*intensity;zeros(N,1)];
                p(1) = patch('Faces', faces, 'Vertices', verts, 'FaceColor', mcolor, 'EdgeColor', 'none', 'FaceAlpha', 'interp', 'AlphaDataMapping', 'none', 'FaceVertexAlphaData', intstep);
                set(get(get(p(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
                verts = [x(:), f(:,1); x(:), f(:,1)-f(:,2)];
                p(2) = patch('Faces', faces, 'Vertices', verts, 'FaceColor', mcolor, 'EdgeColor', 'none', 'FaceAlpha', 'interp', 'AlphaDataMapping', 'none', 'FaceVertexAlphaData', intstep);
                set(get(get(p(2),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
                
            otherwise                
                initfaces = [q, q+1, q+N+1, q+N]; faces = zeros((N-1)*nsteps,4);
                verts = zeros(2*N*nsteps,2);
                intstep = zeros(1,(N-1)*nsteps);
                
                for i = 1:nsteps
                    newfaces = initfaces+2*(i-1)*N;
                    faces((i-1)*(N-1)+1:i*(N-1),:) = newfaces;
                    switch distribution
                        case 'hyperbolic'
                            verts((i-1)*2*N+1:i*2*N,:) = [x(:), f(:)+sgn*(i-1)/nsteps*thickness; x(:), f(:)+sgn*i/nsteps*thickness];
                            intstep(1,(i-1)*(N-1)+1:i*(N-1)) = ones(1,N-1)*intensity/i;
                        case 'summarizing'
                            verts((i-1)*2*N+1:i*2*N,:) = [x(:), f(:); x(:), f(:)+sgn*i/nsteps*thickness];
                            intstep(1,(i-1)*(N-1)+1:i*(N-1)) = ones(1,N-1)*intensity/i;
                        case 'gaussian'
                            verts((i-1)*2*N+1:i*2*N,:) = [x(:), f(:)+sgn*(i-1)/nsteps*thickness; x(:), f(:)+sgn*i/nsteps*thickness];
                            intstep(1,(i-1)*(N-1)+1:i*(N-1)) = ones(1,N-1)*intensity*exp(-((i/nsteps-0.5)^2)/sharpness);
                    end
                    
                end
                if exist('p','var')
                   handlecounter = handlecounter + 1; 
                end
                p(handlecounter) = patch('Faces', faces, 'Vertices', verts, 'FaceColor', mcolor, 'EdgeColor', 'none', 'FaceAlpha', 'flat', 'AlphaDataMapping', 'none', 'FaceVertexAlphaData', intstep');
                set(get(get(p(handlecounter),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
        end

    end
    
    if ~(up||down)
        faces = [q, q+1, q+N+1, q+N];
        verts = [x(:), f(:,1); x(:) f(:,2)];
        p = patch('Faces', faces, 'Vertices', verts, 'FaceColor', mcolor, 'EdgeColor', 'none', 'FaceAlpha', intensity);
        set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    end
       
end

for k = 1:nargout
    if exist('p','var')
        varargout{k} = p;
    else
        varargout{k} = 'Function not active';
    end
    
end



end