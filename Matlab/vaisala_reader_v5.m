function [tvais,mRH,vtmp] = vaisala_reader_v5(filename,LabVIEW_t0_long)
%% read signal
% Vaisala export txt
fileID = fopen(char(strcat('..\Data\43rd experiment SSPv12\Data\',filename,{'_Vaisala.txt'})),'r');
% Read the data stored in the file from Vaisala
%% Number of lines determination
nr = 0;
a = 'a';
while ischar(a)
    a = fgetl(fileID);
    if ischar(a)
        nr = nr+1;
    end
end
fclose(fileID);
fileID = fopen(char(strcat('..\Data\43rd experiment SSPv12\Data\',filename,{'_Vaisala.txt'})),'r');

% Skip the header
header = 8; % Header lines
nr = nr - header;

for k=1:header
     fgetl(fileID);
end

formatSpec = '%f,%f';

% Vector preallocation for speed
tvais(1:nr) = datetime;
mRH(1:nr) = 0;
vtmp(1:nr) = 0;

%% information decoding
for i = 1:nr
    
    mline = fgetl(fileID);
    tvais(i) = datetime(mline(1:19)); % Time series
    
    aux = sscanf(mline(21:end-2),formatSpec);
    mRH(i) = aux(1); % measured relative humidity
    vtmp(i) = aux(2); % temperature of the humidity probe

end

%% vector cropping ... use when the Vaisala export contains older data
relevant = tvais > datetime(LabVIEW_t0_long,'InputFormat','MM/dd/uuuu HH:mm:ss');
tvais = tvais(relevant);
mRH = mRH(relevant);
vtmp = vtmp(relevant);

tvais = seconds(tvais-datetime(LabVIEW_t0_long,'InputFormat','MM/dd/uuuu HH:mm:ss'));


%% Close the files used
fclose(fileID);
