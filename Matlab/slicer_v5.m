function [slices] = slicer_v5(sRH, sfrtmp, tlabv, cpi1, tv, stv, tinit, tsd)
% slicer_v2 takes the whole sequence and makes a cell array of slices which
% correspond to the individual humidities
%   The cell array is returned to the parental code

D = diff(sRH);
CV = abs([D ; 0]) >= 15; % change vector, marks the position where the changes in humidity occur (marks the last point before the change)

counter = 1;
last_separator = sum(CV);
i = 1; j = 1; fi = 1;

%% RH stairs separation
while counter <= last_separator    
    if CV(i) == 1
        slices{counter}(1:j,1:4) = [tlabv(fi:i), cpi1(fi:i), tv(fi:i), stv(fi:i)];
        counter = counter + 1;
        fi = i + 1;
        j = 0;
    end
    i = i + 1;
    j = j + 1;
end

%% Save the last indexes for the cropping later
si = i; % si is the index of the first point of the next slice


%% 1st cycle set cropping
D2  = diff(slices{1}(:,4));
CV2 = abs([D2 ; 0]) >= 0.01; % change vector, marks the position where the changes in hydrogen concentration occur (marks the last point before the change)

counter = 1;
j = 20000; % with no training H2 pulses applied, use j = 1; % with some training pulses, put j equal to some point after the training sequence

% find the first H2 step
while counter <= 1
    if CV2(j) == 1
        tfh = slices{1}(j+1,1); % time of the first point exposed to H2     
        counter = counter + 1;
    end
    j = j + 1;
end

% crop at the beginning
tf = tfh - tinit*60;    % ... tfh is time of the beginning of the first hydrogen pulse, tf is the new start time

D3 = abs(slices{1}(:,1)-tf);
[smd, smdp] = min(D3);

slices{1} = slices{1}(smdp:end,:);
    


%% Pre-TR cycle set
%slices{end+1} = slices{end}; 

D4  = diff(sfrtmp);
CV4 = abs([D4 ; 0]) >= 0.05; % change vector, marks the position where the changes in hydrogen concentration occur (marks the last point before the change)

counter = 1;
j = 1;
counter_final = 3;
% find the first TR point
while counter <= counter_final
    if CV4(j) == 1
        lj = j; % the last index with the low temperature   
        counter = counter + 1;
    end
    j = j + 1;
end

slices{end+1} = [tlabv(si:lj), cpi1(si:lj), tv(si:lj), stv(si:lj)]; 


%% Post-TR cycle set
while counter <= counter_final + 1
    if CV4(j) == 1
        lj = j; % the last index with the high temperature   
        counter = counter + 1;
    end
    j = j + 1;
end

slices{end+1} = [tlabv(j:end), cpi1(j:end), tv(j:end), stv(j:end)];

D5  = diff(slices{end}(:,4));
CV5 = abs([D5 ; 0]) >= 0.01; % change vector, marks the position where the changes in hydrogen concentration occur (marks the last point before the change)

counter = 1;
j = 1;

% find the first H2 step
while counter <= 1
    if CV5(j) == 1
        tfh = slices{end}(j+1,1); % time of the first point exposed to H2     
        counter = counter + 1;
    end
    j = j + 1;
end

% crop at the beginning
tf = tfh - tinit*60;    % ... tfh is time of the beginning of the first hydrogen pulse, tf is the new start time

D6 = abs(slices{end}(:,1)-tf);
[smd, smdp] = min(D6);

slices{end} = slices{end}(smdp:end,:);

% crop at the end
D7  = diff(slices{end}(:,4));
CV7 = abs([D7 ; 0]) >= 0.01; % change vector, marks the position where the changes in hydrogen concentration occur (marks the last point before the change)

counter = 1;
j = length(slices{end});

% find the last H2 step
while counter <= 1
    if CV7(j) == 1
        tlh = slices{end}(j+1,1); % time of the last point + 1 exposed to H2 (== first not exposed point)
        counter = counter + 1;
    end
    j = j - 1;
end
lj = j+2; % index of the first not exposed point

% crop at the end
tl = tlh + tsd*60;    % ... tf is the new end time

D8 = abs(slices{end}(:,1)-tl);
[smd, smdp] = min(D8);
slices{end} = slices{end}(1:smdp,:);


end

