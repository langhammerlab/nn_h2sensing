function [fh] = visualisedt_v15(tinsp1,cp1,tlabv,tv,RH,totf,stmp,xlim,Quantity)
% Visualize measured data

fh = figure;

%% Signal + concentration graph
subplot(2,1,1)
yyaxis left
plot(tinsp1/60,cp1);

title('Overview plot of the experiment at 80 �C');
xlabel('{\it T} (min)','fontweight','bold')
if strcmp('Extinction',Quantity)
    ylabel('Extinction (a.u.)','fontweight','bold')
else
    ylabel('Centroid position (nm)','fontweight','bold')
end

yyaxis right
plot(tlabv/60,tv);
ylabel('Hydrogen concentration (%)','fontweight','bold')
set(gca,'XLim', xlim);

%% Temperature, RH and Total flow graph
subplot(2,1,2)
yyaxis left
plot(tlabv/60,RH,tlabv/60,stmp);
xlabel('{\it T} (min)','fontweight','bold')
ylabel('RH (%), Sample temperature (� C)','fontweight','bold')

yyaxis right
plot(tlabv/60,totf);
ylabel('Total flow (ml/min)','fontweight','bold')
legend('RH (%)','Sample temperature (� C)','Total flow (ml/min)','Location','east');
set(gca,'XLim', xlim);
end

