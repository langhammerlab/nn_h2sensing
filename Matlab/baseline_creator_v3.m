function [baselines] = baseline_creator_v3(slices,avg_factor)

for i = 1:length(slices)
    D  = diff(slices{i}(:,4));
    CV = abs([D ; 0]) >= 0.01; % change vector, marks the position where the changes in hydrogen concentration occur (marks the last point before the change)
    j = 1;
    while CV(j) == 0
        j = j + 1;
    end
    fj = j;
    lj = length(slices{i}(:,4));
    
    %% baseline construction
    baselines{i} = zeros([1,lj]); % preallocation
    baselines{i}(1:fj) = linspace(mean(slices{i}(1:avg_factor,2)),mean(slices{i}(fj-avg_factor+1:fj,2)),fj);
    baselines{i}(fj:lj) = linspace(mean(slices{i}(fj-avg_factor+1:fj,2)),mean(slices{i}(end-avg_factor+1:end,2)),lj-fj+1);     
end

end

