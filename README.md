# NN_H2Sensing

The official repository containing the machine learning and standard analysis tools used to analyze the nanoplasmonic response of hydrogen gas sensors, introduced in "Neural network enabled nanoplasmonic hydrogen sensors with 100 ppm limit of detection in humid air".
To demo the software, please follow the instructions found in "Instructions for Demo - SA.txt" and "Instructions for Demo - ML.txt".